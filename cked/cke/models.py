from django.db import models
from ckeditor.fields import RichTextField


class Tbl(models.Model):
    content = RichTextField(config_name='awesome_ckeditor')
